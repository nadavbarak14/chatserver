import flask
import MessageHandler
from ChatConfig.API import SendMessage, GetMessages, CreateRoom, SelectRoom, GetRoomsList
import RoomsHandler
from flask import request, jsonify, json


app = flask.Flask(__name__)


@app.route('/{}'.format(SendMessage.path), methods=[SendMessage.type])
def send_message():
    username = request.values.get('user')
    text = request.values.get('text')

    room = request.cookies.get('room')
    if not room:
        response = app.response_class(
            status=404,
            mimetype='application/json'
        )
        return response

    MessageHandler.incoming_message(username, text, room)

    response = app.response_class(
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/{}'.format(GetMessages.path), methods=[GetMessages.type])
def get_messages():
    room = request.cookies.get('room')
    if not room:
        response = app.response_class(
            status=404,
            mimetype='application/json'
        )
        return response
    response = app.response_class(
        response=MessageHandler.get_messages(room),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/{}'.format(CreateRoom.path), methods=[CreateRoom.type])
def create_room():
    room_name = request.values.get('name')

    print (room_name)

    # if room name already exists select the room
    if room_name in RoomsHandler.get_rooms():
        response = app.response_class(
            status=200,
            mimetype='application/json'
        )

        response.set_cookie('room', room_name)
        return response

    RoomsHandler.add_new_room(room_name)

    response = app.response_class(
        status=200,
        mimetype='application/json'
    )

    response.set_cookie('room', room_name)

    return response


@app.route('/{}'.format(GetRoomsList.path), methods=[GetRoomsList.type])
def get_rooms():

    response = app.response_class(
        response=RoomsHandler.get_rooms(),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/{}'.format(SelectRoom.path), methods=[SelectRoom.type])
def select_room():
    print (request.data)
    room_name = request.values.get('name')
    print(room_name)


    if not room_name or room_name not in RoomsHandler.get_rooms():
        response = app.response_class(
            status=404,
            mimetype='application/json'
        )
        return response

    response = app.response_class(
        status=200,
        mimetype='application/json'
    )

    response.set_cookie('room', room_name)

    return response


def run_server(**kwargs):
    app.run(**kwargs)
