from collections import namedtuple

import sqlite3

Message = namedtuple("Message", "username text group")


def append_message_to_db(username, text, room_name):
    conn = sqlite3.connect('../messages.db') # TODO : change filename to be in config
    conn.execute("INSERT INTO MESSAGES (USERNAME,USERNAME_TEXT, ROOM_NAME) \
          VALUES ('{}','{}', '{}')".format(username, text, room_name))
    conn.commit()
    conn.close()


def get_room_message_from_db(room_name):
    messages = []
    conn = sqlite3.connect('../messages.db')  # TODO : change filename to be in config
    cursor = conn.execute("SELECT * FROM MESSAGES WHERE ROOM_NAME=?", (room_name,))

    for row in cursor:
        messages.append(Message(*row))

    return messages


def incoming_message(username, text, room_name):
    append_message_to_db(username, text, room_name)


def get_messages(room_name):
    messages = get_room_message_from_db(room_name)
    return "\n".join(["{}:{}".format(message.username, message.text) for message in messages])
