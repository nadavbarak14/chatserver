import sqlite3


def add_new_room(room_name):
    conn = sqlite3.connect('../rooms.db')  # TODO : change filename to be in config
    conn.execute("INSERT INTO ROOMS (NAME) \
              VALUES ('{}')".format(room_name))
    conn.commit()
    conn.close()


def get_rooms():
    rooms = []
    conn = sqlite3.connect('../rooms.db')  # TODO : change filename to be in config
    cursor = conn.execute("SELECT NAME from ROOMS")
    for row in cursor:
        rooms.append(row[0])

    return " ".join(rooms)


if __name__ == '__main__':
    add_new_room("test1")
    print (get_rooms())
