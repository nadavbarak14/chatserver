class Option:
    def __init__(self, path, type):
        self._path = path
        self._type = type

    @property
    def path(self):
        return self._path

    @property
    def type(self):
        return self._type


SendMessage = Option('messages/send', "POST")
GetMessages = Option("messages", "GET")
CreateRoom = Option("rooms/create", "POST")
SelectRoom = Option("rooms/select", "POST")
GetRoomsList = Option("rooms/all", "GET")
