from threading import Thread
import time
from ChatAPI import get_messages, send_message, get_rooms, create_room, select_room, set_cookies
import os


class Client:
    def __init__(self, username, polling_interval):
        self._username = username
        self._polling_interval = polling_interval

    def _clear_output_screen(self):
        os.system("cls")

    def _poll_messages(self):
        while True:
            self._clear_output_screen()
            print (get_messages())
            time.sleep(self._polling_interval)

    def _select_room(self):
        rooms = get_rooms()

        print (rooms)

        user_input = input("Enter room name, or type a new name to create a room\n")

        if user_input in rooms:
            response = select_room(user_input)

        else:
            response = create_room(user_input)

        return response.cookies.get_dict()

    def start(self):
        cookies = self._select_room()
        set_cookies(cookies)
        t = Thread(target=self._poll_messages)
        t.start()

        while True:
            user_text = input(">>>  ")
            send_message(self._username, user_text)


def start_client(username, polling_interval):
    c = Client(username, polling_interval)
    c.start()
