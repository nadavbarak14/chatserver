import requests
from ChatConfig.API import SendMessage, GetMessages, SelectRoom, GetRoomsList, CreateRoom


class ConnectionErrorException(Exception):
    pass


SERVER_ADDRESS = "127.0.0.1"

cookies = {}


def set_cookies(new_cookies):
    global cookies
    cookies = new_cookies


def get_send_function(request_type):
    if "POST" == request_type:
        return requests.post
    return requests.get


def send_request(message_path, message_type, message_payload):
    final_url = r"http://{}/{}".format(SERVER_ADDRESS, message_path)

    response = get_send_function(message_type)(final_url, data=message_payload, cookies=cookies)
    return response


def get_messages():
    payload = {}

    respone = send_request(GetMessages.path, GetMessages.type, payload)
    if respone.status_code != 200:
        raise ConnectionErrorException

    return respone.text


def send_message(username, text):
    payload = {'user': username, 'text': text}

    return send_request(SendMessage.path, SendMessage.type, payload)


def get_rooms():
    payload = {}

    response = send_request(GetRoomsList.path, GetRoomsList.type, payload)
    return response.text.split(" ")


def create_room(room_name):
    payload = {'name': room_name}

    return send_request(CreateRoom.path, CreateRoom.type, payload)


def select_room(room_name):
    payload = {'name': room_name}

    return send_request(SelectRoom.path, SelectRoom.type, payload)


if __name__ == '__main__':
    print (get_messages())
    print (send_message("bob", "hello"))
