from CliClient import start_client
from config import CLIENT_POLLING_INTERVAL


def get_username():
    username = input("Enter username: ")
    return username


start_client(get_username(), CLIENT_POLLING_INTERVAL)

